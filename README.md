

# Dog Api con React Redux
    
  - Busca imagenes de perros Random o por Raza
  - Consume imagenes y listado de razas de Dog Api
  - Solo operaciones GET Y DELETE, Delete solo sobre el store de redux

Live https://react-dopapi.web.app/


### Instalación


```sh
$ npm install 
$ npm start
```


