import { combineReducers } from 'redux';
import {imagenes} from './imagesReducer';
import {breeds} from './breedsReducer';

export const reducers = combineReducers({
    imagenesDog: imagenes,
    breedsDog: breeds
})