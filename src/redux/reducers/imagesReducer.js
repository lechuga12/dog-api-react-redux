import {
    FETCH_IMG_REQUEST,
    FETCH_IMG_SUCCESS,
    FETCH_IMG_ERROR,
    IMG_DELETE_SUCCESS,
    IMG_DELETE_ERROR
} from '../types';

const initialState = {
    imagenes: [],
    error:false,
    loading: false,
}

export function imagenes(state = initialState, action){
    switch(action.type){
        case FETCH_IMG_REQUEST:
            return {
                ...state,
                imagenes:[],
                error:false,
                loading: true
            }
        case FETCH_IMG_SUCCESS:
            return{
                ...state,
                loading: false,
                imagenes: action.payload.slice()

            }
        case FETCH_IMG_ERROR:
            return {
                ...state,
                error:true,
                loading:false
            }
        case IMG_DELETE_SUCCESS:
            return {
                ...state,
                imagenes: state.imagenes.filter( i => i !== action.payload)
            }
        case IMG_DELETE_ERROR:
            return {
                ...state
            }
        default:
            return state
    }
}
