import{
    FETCH_BREEDS_REQUEST,
    FETCH_BREEDS_SUCCESS,
    FETCH_BREEDS_ERROR
} from '../types';

const initialState = {
    breeds:[],
    loading:false,
    error:false
}

export function breeds( state = initialState,action){
    switch(action.type){
        case FETCH_BREEDS_REQUEST:
            return {
                ...state,
                loading:true,
                error:false
            }
        case FETCH_BREEDS_SUCCESS:
            return {
                ...state,
                loading:false,
                breeds: action.payload
            }
        case FETCH_BREEDS_ERROR:
            return {
                ...state,
                loading:false,
                error:true
            }
        default:
            return state
    }
}