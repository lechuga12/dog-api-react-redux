import {
    FETCH_IMG_REQUEST,
    FETCH_IMG_SUCCESS,
    FETCH_IMG_ERROR,
    IMG_DELETE_SUCCESS,
    IMG_DELETE_ERROR
} from '../types';

import axios from 'axios';



export function getImagesAction(cantidad = 40,raza = ''){
    return async (dispatch) => {

        dispatch(startFetchImages());
        try{
            let url = 
                ( raza === ''?
                    `https://dog.ceo/api/breeds/image/random/${cantidad}`
                    :
                    `https://dog.ceo/api/breed/${raza}/images/random/${cantidad}`    
                )

            let { data } =  await axios.get(url)
            dispatch(successFetchImages(data.message))
        }catch(e){
            dispatch(errorFetchImages());
            throw e
        }
    }
}

export function deleteImgAction( url ){
    return  dispatch => {
        //  Se va a borrar del store de redux, la api no permite eliminar elementos
        // igual es isi borrar xd
        try{
            //  aqui deberia ir la llamada delete a la api con el id del elemento
            // su await obvios
            dispatch(deleteImgSuccess(url));
        }catch(e){
            // si falla el fetch igual se maneja 
            dispatch(deleteImgError)
        }
    }
}

const startFetchImages = () => {
    return { type: FETCH_IMG_REQUEST}
}

const successFetchImages = (imagenes) => {
    return { type: FETCH_IMG_SUCCESS, payload: imagenes}
}

const errorFetchImages = () => {
    return { type: FETCH_IMG_ERROR}
}

const deleteImgSuccess = ( url ) => {
    return { type: IMG_DELETE_SUCCESS , payload: url}
}
const deleteImgError = () => {
    return { type: IMG_DELETE_ERROR} 
}