import{
    FETCH_BREEDS_REQUEST,
    FETCH_BREEDS_SUCCESS,
    FETCH_BREEDS_ERROR
} from '../types';

import axios from 'axios';

export function getBreedsAction(){
    return async (dispatch) => {
        dispatch(startFetchBreeds());
        try{
            let {data} = await axios.get('https://dog.ceo/api/breeds/list/all');
            dispatch(successFetchBreeds(Object.keys(data.message)));
        }catch(e){
            dispatch(errorFetchBreeds());
            throw e
        }
    }
}

const startFetchBreeds = () => {
    return { type:FETCH_BREEDS_REQUEST}
}
const successFetchBreeds = (breeds) => {
    return { type: FETCH_BREEDS_SUCCESS, payload: breeds}
}
const errorFetchBreeds = () => {
    return { type: FETCH_BREEDS_ERROR}
}