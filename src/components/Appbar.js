import React from 'react';

import {Navbar,Nav,Container} from 'react-bootstrap';
function Appbar(){

    return (
        <Navbar bg="dark" variant="dark" expand="lg" id="appbar" >
            <Container>
                <Navbar.Brand>Images</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbar-collapse-control" />

                <Navbar.Collapse id="navbar-collapse-control">
                    <Nav className="mr-auto">
                        <Nav.Link href="https://dog.ceo/dog-api/" target="_blank">DogApi </Nav.Link>
                        <Nav.Link>Nada </Nav.Link>
                        <Nav.Link>Vacío </Nav.Link>
                        <Nav.Link>Blank </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
export default Appbar;