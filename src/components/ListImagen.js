import React,{Fragment} from 'react';
import {useSelector} from 'react-redux';


import { CardColumns, Spinner,Row,Col,Container,Alert} from 'react-bootstrap';
import Imagen from './Imagen';

function ListImagen(){

    const loading = useSelector( state => state.imagenesDog.loading);
    const imagenes = useSelector( state => state.imagenesDog.imagenes);
    const error = useSelector( state => state.imagenesDog.error);
    return(
        <Fragment>
            {
                loading || error ?
                    <Container>
                        <Row className="justify-content-center align-items-center" style={{height:300}}>
                            {
                                loading ?
                                    <Col xs={2}>
                                        <Spinner animation="border" role="status" style={{width: '5rem',height: '5rem'}}>
                                            <span className="sr-only">Loading...</span>
                                        </Spinner>
                                    </Col>
                                    :
                                    null
                            }

                            {
                                error ?
                                    <Col>
                                        <Alert  className="text-center" variant='danger'>
                                            No se ha podido cargar las imagenes
                                        </Alert>
                                    </Col>
                                    :
                                    null

                            }
                        </Row>
                    </Container>
                    :
                    null
            }
            <CardColumns className="mt-4">
                {
                    imagenes.map( (item,id) => (
                        <Imagen key={id} dataImagen={item}/>
                    ))
                }
            </CardColumns>
        </Fragment>
    )
}
export default ListImagen;