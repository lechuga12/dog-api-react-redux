import React,{useEffect, useState} from 'react';

import {useSelector,useDispatch} from 'react-redux'; 
import {getBreedsAction} from '../redux/actions/breedsActions';
import {getImagesAction} from '../redux/actions/imagesAction';


import Select from 'react-select'
import {Row, Col,Spinner,Alert} from 'react-bootstrap';

function SearchImage(){
    const dispatch = useDispatch();
 
    const breeds = useSelector( state => state.breedsDog.breeds);
    const error  = useSelector( state => state.breedsDog.error);
    const loading = useSelector( state => state.breedsDog.loading);

    const [cantidad, setCantidad] = useState(10);
    const [selectBreed, setSelectBreed] = useState('');

    const optionsBreeds = breeds.map( i => ({value:i,label:i}))
    const optionsCant = [
        {value:10,label:'10'},
        {value:20,label:'20'},
        {value:40,label:'40'}
    ]    
    const handleChangeSelect = (e) => {
        setSelectBreed(e.value)
        dispatch(getImagesAction(cantidad,e.value));
    }

    const handleChangeCantidad = e => {
        setCantidad(e.value);
        dispatch(getImagesAction(e.value,selectBreed))
    }

    useEffect( () => {
        dispatch(getBreedsAction())
        dispatch(getImagesAction(cantidad))

    },[])
    return(
        <Row className="align-items-center justify-content-center search-content" style={{height:400}}>
            <Col xs={12} className="text-center" >
                <h1 className="display-2">Dogs</h1>
                <p>Obtiene imagenes de DogApi</p>
                <span>Get con Redux Thunk, Eliminar solo se aplicara sobre el Store de Redux</span>
            </Col>


            <Col xs={8} lg={6} className="ml-3 text-center" style={{color:'black'}}>
                { loading ? 
                    <Spinner animation="border" variant="light"/> 
                    : 
                    <Select  
                        className="my-2"
                        options={optionsBreeds}  
                        onChange={handleChangeSelect}    
                    />
                }
                {
                    error ? 
                        <Alert variant="danger" >
                            <Alert.Heading>Ocurrio un error</Alert.Heading>
                            <p>
                            No se ha podido obtener la lista de razas de los perros :(
                            </p>
                        </Alert>
                        :
                        null
                }
                
            </Col>
            <Col xs={2} lg={1} style={{color:'black'}}>
                <Select  
                    placeholder={cantidad}
                    options={optionsCant}
                    onChange={ handleChangeCantidad}
                    className="my-2"
                />
            </Col>
        </Row>
    )
}
export default SearchImage;