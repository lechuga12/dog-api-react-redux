import React from 'react'

import {Card} from 'react-bootstrap'; 
import {deleteImgAction} from '../redux/actions/imagesAction';

import { useDispatch} from 'react-redux';

const Imagen = ({dataImagen}) => {
    const dispatch = useDispatch();

    const handleDelete = () => {
        dispatch(deleteImgAction(dataImagen));
    }
    const handleImg = () => {
        window.open(dataImagen, "_blank")
    }
    return(
        <Card  >
            <div 
                className="icon-delete"  
                onClick={() => handleDelete()}
            >
                <ion-icon size="large" name="trash"  ></ion-icon>
            </div>
            <Card.Img  onClick={ () => handleImg()} src={dataImagen} alt="card" />
        </Card>
    )
}
export default Imagen;