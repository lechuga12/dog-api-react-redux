import React from 'react';
import './App.css';

import Appbar from './components/Appbar';
import SearchImage from './components/SearchImage';
import ListImage from './components/ListImagen';

import {Container} from 'react-bootstrap';

function App() {
  return (
    <>
      <Appbar/> 
      
      <SearchImage/>

      <Container>
        <ListImage/>
      </Container>
      
    </>
    
  );
}

export default App;
